from django.db import models


# Create your models here.


class Room(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название помещения")
    schema = models.ImageField(upload_to="schema/rooms")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Комната"
        verbose_name_plural = "Комнаты"


class Floor(models.Model):
    floor = models.IntegerField(verbose_name="Номер этажа")
    rooms = models.ManyToManyField(Room, related_name='floor')
    schema = models.ImageField(upload_to="schema/floor")
    #schema_2d = models.ImageField(upload_to="schema/floor", null=True, blank=True)

    def __str__(self):
        return f"{self.floor} этаж в {self.house.first()}"

    class Meta:
        verbose_name = "Этаж"
        verbose_name_plural = "Этажы"


class House(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название дома')
    floors = models.ManyToManyField(Floor, related_name='house')
    new = models.BooleanField(default=True)
    img = models.ImageField(upload_to='house')

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = "Дом"
        verbose_name_plural = "Дома"


class TypeSensor(models.Model):
    name = models.CharField(max_length=255, verbose_name="Тип датчика")
    img = models.ImageField(upload_to='sensors')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Сенсор"
        verbose_name_plural = "Сенсоры"


class Point(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, verbose_name="Название датчика")
    x = models.IntegerField(verbose_name='x')
    y = models.IntegerField(verbose_name='y')
    line_x = models.IntegerField(verbose_name="line_x", null=True, blank=True)
    line_y = models.IntegerField(verbose_name="line_y", null=True, blank=True)
    image = models.CharField(max_length=255, verbose_name="Изображение датчика")
    param_1 = models.CharField(max_length=255, verbose_name="Параметр 1", null=True, blank=True)
    param_2 = models.CharField(max_length=255, verbose_name="Параметр 2", null=True, blank=True)
    param_3 = models.CharField(max_length=255, verbose_name="Параметр 3", null=True, blank=True)
    param_4 = models.CharField(max_length=255, verbose_name="Параметр 4", null=True, blank=True)
    param_5 = models.CharField(max_length=255, verbose_name="Параметр 5", null=True, blank=True)
    param_6 = models.CharField(max_length=255, verbose_name="Параметр 6", null=True, blank=True)
    param_7 = models.CharField(max_length=255, verbose_name="Параметр 7", null=True, blank=True)

    def __str__(self):
        return f"{self.name} в {self.room}"

    class Meta:
        verbose_name = "Датчик"
        verbose_name_plural = "Датчики"
