from .views import *
from django.urls import path


urlpatterns = [
    path('menu', menu, name='menu'),
    path('', loginUser, name='login'),
    path('schema_doma_<int:pk>', schema, name='schema'),
    path('save', save, name='save'),
    path('select_floor', selectFloor, name='select_floor'),
    path('get_rooms', getRooms, name='get_rooms'),
    path('remove', removeboll, name='removeboll'),
    path('remove_line', removeLine, name='removeLine'),
    path('add_line', addLine, name='addLine'),
    path('get_statistic', get_data, name='get_data'),
   # path('change_select', change_select, name='change_select'),
    path('get_info_sensor', get_info_sensor, name='get_info_sensor'),
    path('save_param', save_param, name='save_param'),
]