from django.contrib import admin
from .models import *
# Register your models here.


admin.site.register(House)
admin.site.register(Floor)
admin.site.register(Room)
admin.site.register(TypeSensor)
admin.site.register(Point)
