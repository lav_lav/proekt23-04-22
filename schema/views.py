from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.http import HttpResponseForbidden
from django.http import JsonResponse
# import numpy as np
from .models import *
import driver_api.corall_db_access as cdb

conn = cdb.DbAccess()
conn.openConnection('c:/PROJECT/DJANGOPRO/Schemaa/driver_api/corall_db.sqlite')

def loginUser(request):
    if request.user.is_authenticated:
        return redirect('menu')
    if request.method == 'POST':
        user = authenticate(request, username=request.POST.get('username'), password=request.POST.get('password'))
        if not user:

            return JsonResponse(status=403, data={'detail': 'Неверное имя пользователя или пароль'})
        login(request, user)
        return JsonResponse(status=200, data={'status': True})
    return render(request, 'login.html')


def menu(request):
    if request.user.is_authenticated:
        house = House.objects.all()
        print(house)
        return render(request, 'index.html', {'house': house})
    return HttpResponseForbidden("Необходимо авторизироваться")


def schema(request, pk):
    if request.user.is_authenticated:
        labels = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
        data = [65, 60, 68, 54, 63, 60, 60]
        house = House.objects.get(pk=pk)
        house.new = False
        house.save()
        floor = house.floors.all().order_by('floor')
        rooms = floor[0].rooms.all()
        arr = []
        for room in rooms:
            for i in Point.objects.filter(room=room):
                arr.append(i)
        return render(request, 'shema.html', {'pk': pk, "floor": floor, 'point': arr, "labels": labels, "data": data})
    return HttpResponseForbidden("Необходимо авторизироваться")


def save(request):
    print(request.POST)
    house = House.objects.get(id=request.POST.get('id_house'))
    floor = house.floors.filter(floor=request.POST.get('floor')).first()
    x = round(int(request.POST.get('x')), 0)
    y = round(int(request.POST.get('y')), 0)
    img = request.POST.get('url')
    name = request.POST.get('name')
    room = Room.objects.get(id=request.POST.get('room'))
    if not Point.objects.filter(room=room, x=x - 20, y=y - 20).first():
        Point.objects.create(room=room, x=x - 20, y=y - 20, image=img, name=name).save()
    arr = []
    for room in floor.rooms.all():
        for p in Point.objects.filter(room=room):
            arr.append({
                'id': p.id,
                'name': p.name,
                'room': p.room.name,
                'x': p.x,
                'y': p.y,
                'line_x': p.line_x,
                'line_y': p.line_y,
                'url': p.image
            })
    return JsonResponse({'arr': arr})


def removeboll(request):
    point = Point.objects.get(id=request.POST.get('id'))
    point.delete()
    house = House.objects.get(id=request.POST.get('id_house'))
    floor = house.floors.filter(floor=request.POST.get('floor')).first()
    arr = []
    for room in floor.rooms.all():
        for p in Point.objects.filter(room=room):
            arr.append({
                'id': p.id,
                'name': p.name,
                'room': p.room.name,
                'x': p.x,
                'y': p.y,
                'line_x': p.line_x,
                'line_y': p.line_y,
                'url': p.image
            })
    return JsonResponse({'arr': arr})


def selectFloor(request):
    house = House.objects.get(id=request.POST.get('id_house'))
    floor = house.floors.filter(floor=request.POST.get('floor')).first()
    arr = []
    for room in floor.rooms.all():
        for p in Point.objects.filter(room=room):
            arr.append({
                'id': p.id,
                'name': p.name,
                'room': p.room.name,
                'x': p.x,
                'y': p.y,
                'line_x': p.line_x,
                'line_y': p.line_y,
                'url': p.image
            })
    return JsonResponse({'img': floor.schema.url, 'arr': arr})


def getRooms(request):
    house = House.objects.get(id=request.POST.get('id_house'))
    floor = house.floors.filter(floor=request.POST.get('floor')).first()
    arr = [{'id': i.id,
            'room': i.name} for i in floor.rooms.all()]

    return JsonResponse({'rooms': arr})


def removeLine(request):
    house = House.objects.get(id=request.POST.get('id_house'))
    floor = house.floors.filter(floor=request.POST.get('floor')).first()
    point = Point.objects.get(id=request.POST.get('id'))
    point.line_x = None
    point.line_y = None
    point.save()
    arr = []
    for room in floor.rooms.all():
        for p in Point.objects.filter(room=room):
            arr.append({
                'id': p.id,
                'name': p.name,
                'room': p.room.name,
                'x': p.x,
                'y': p.y,
                'line_x': p.line_x,
                'line_y': p.line_y,
                'url': p.image
            })
    return JsonResponse({'arr': arr})


def addLine(request):
    print(request.POST)
    house = House.objects.get(id=request.POST.get('id_house'))
    floor = house.floors.filter(floor=request.POST.get('floor')).first()
    point = Point.objects.get(id=request.POST.get('id'))
    point.line_x = int(round(int(request.POST.get('x'))))
    point.line_y = int(round(int(request.POST.get('y'))))
    point.save()
    arr = []
    for room in floor.rooms.all():
        for p in Point.objects.filter(room=room):
            arr.append({
                'id': p.id,
                'name': p.name,
                'room': p.room.name,
                'x': p.x,
                'y': p.y,
                'line_x': p.line_x,
                'line_y': p.line_y,
                'url': p.image
            })
    return JsonResponse({'arr': arr})


def get_info_sensor(request):
    id = request.POST.get('id')
    point = Point.objects.get(id=id)
    return JsonResponse(status=200, data={'id': point.id,
                                          "name": point.name,
                                          "img": point.image,
                                          "room": point.room.name,
                                          'param1': point.param_1,
                                          'param2': point.param_2,
                                          'param3': point.param_3,
                                          'param4': point.param_4,
                                          'param5': point.param_5,
                                          'param6': point.param_6,
                                          'param7': point.param_7,
                                          })

def save_param(request):
    id = request.POST.get('id')
    point = Point.objects.get(id=id)
    point.param_1 = request.POST.get('param1')
    point.param_2 = request.POST.get('param2')
    point.param_3 = request.POST.get('param3')
    point.param_4 = request.POST.get('param4')
    point.param_5 = request.POST.get('param5')
    point.param_6 = request.POST.get('param6')
    point.param_7 = request.POST.get('param7')
    point.save()
    return JsonResponse(status=200, data={'status': True})


#################################################### Работа с графиками ##################################################################################


def set_random_data(n):
    import random
    freq = conn.get_sensor_data(sensor_id=9)
    dd = [d['temp'] for d in freq]
    return [d['temp'] for d in freq]
   # return [random.randint(0, 500) for _ in range(n)]


def get_data(request):
    id = request.POST.get('id')
    point = Point.objects.get(id=id)
   # labels = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

    freq = conn.get_sensor_data(sensor_id=9)
    labels = [d['frequency'] for d in freq]

    data1 = set_random_data(len(freq))
    data2 = set_random_data(len(freq))
    data3 = set_random_data(len(freq))
    data4 = set_random_data(len(freq))
    return JsonResponse({"data1": data1, "data2": data2, "data3": data3, "data4": data4,
                         "labels1": labels, "labels2": labels, "labels3": labels, "labels4": labels})


def change_select(request):
    print(request.POST)
    id = request.POST.get('id')
    chart = request.POST.get('chart')
    select = request.POST.get('select')
    point = Point.objects.get(id=id)
    if select == "День":
        print(select)
    elif select == "Месяц":
        print(select)
    elif select == "Год":
        print(select)
    labels = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    data = set_random_data(len(labels))
    return JsonResponse(status=200, data={"data": data, "labels": labels})
