var myChart1 = null
var myChart2 = null
var myChart3 = null
var myChart4 = null

Chart.defaults.LineWithShadow = Chart.defaults.line;
Chart.controllers.LineWithShadow = Chart.controllers.line.extend({
    draw: function (ease) {
        Chart.controllers.line.prototype.draw.call(this, ease);
        var ctx = this.chart.ctx;
        ctx.save();
        ctx.shadowColor = "rgba(0,0,0,0.15)";
        ctx.shadowBlur = 10;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 10;
        ctx.responsive = true;
        ctx.stroke();
        Chart.controllers.line.prototype.draw.apply(this, arguments);
        ctx.restore();
    }
});
var rootStyle = getComputedStyle(document.body);
var themeColor2 = rootStyle.getPropertyValue("--theme-color-2").trim();
var themeColor2_10 = rootStyle
    .getPropertyValue("--theme-color-2-10")
    .trim();
var primaryColor = rootStyle.getPropertyValue("--primary-color").trim();
var foregroundColor = rootStyle
    .getPropertyValue("--foreground-color")
    .trim();
var separatorColor = rootStyle.getPropertyValue("--separator-color").trim();

var chartTooltip = {
    backgroundColor: foregroundColor,
    titleFontColor: primaryColor,
    borderColor: separatorColor,
    borderWidth: 0.5,
    bodyFontColor: primaryColor,
    bodySpacing: 10,
    xPadding: 15,
    yPadding: 15,
    cornerRadius: 0.15,
    displayColors: false
};

function drawChart1(labels, data) {
    var chartsensors = document.getElementById("chartsensors").getContext("2d");
    if (myChart1 !== null) {
        myChart1.destroy()
    }
    myChart1 = new Chart(chartsensors, {
        type: "LineWithShadow",
        options: {
            plugins: {
                datalabels: {
                    display: false
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [
                    {
                        gridLines: {
                            display: true,
                            lineWidth: 1,
                            color: "rgba(0,0,0,0.1)",
                            drawBorder: false
                        },
                        ticks: {
                            beginAtZero: true,
                            stepSize: parseInt(((Math.max(...data) - Math.min(...data)) / 4).toFixed(2)),
                            min: Math.min(...data),
                            max: Math.max(...data),
                            padding: 0
                        }
                    }
                ],
                xAxes: [
                    {
                        gridLines: {
                            display: false
                        }
                    }
                ]
            },
            legend: {
                display: false
            },
            tooltips: chartTooltip
        },
        data: {
            labels: labels,
            datasets: [
                {
                    label: "",
                    data: data,
                    borderColor: themeColor2,
                    pointBackgroundColor: foregroundColor,
                    pointBorderColor: themeColor2,
                    pointHoverBackgroundColor: themeColor2,
                    pointHoverBorderColor: foregroundColor,
                    pointRadius: 4,
                    pointBorderWidth: 2,
                    pointHoverRadius: 5,
                    fill: true,
                    borderWidth: 2,
                    backgroundColor: themeColor2_10
                }
            ]
        }
    });
}

function drawChart2(labels, data) {
    var chart_stat = document
        .getElementById("chart_stat")
        .getContext("2d");
    if (myChart2 !== null) {
        myChart2.destroy()
    }
    myChart2 = new Chart(chart_stat, {
        type: "LineWithShadow",
        options: {
            plugins: {
                datalabels: {
                    display: false
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [
                    {
                        gridLines: {
                            display: true,
                            lineWidth: 1,
                            color: "rgba(0,0,0,0.1)",
                            drawBorder: false
                        },
                        ticks: {
                            beginAtZero: true,
                            stepSize: parseInt(((Math.max(...data) - Math.min(...data)) / 4).toFixed(2)),
                            min: Math.min(...data),
                            max: Math.max(...data),
                            padding: 0
                        }
                    }
                ],
                xAxes: [
                    {
                        gridLines: {
                            display: false
                        }
                    }
                ]
            },
            legend: {
                display: false
            },
            tooltips: chartTooltip
        },
        data: {
            labels: labels,
            datasets: [
                {
                    label: "",
                    data: data,
                    borderColor: themeColor2,
                    pointBackgroundColor: foregroundColor,
                    pointBorderColor: themeColor2,
                    pointHoverBackgroundColor: themeColor2,
                    pointHoverBorderColor: foregroundColor,
                    pointRadius: 4,
                    pointBorderWidth: 2,
                    pointHoverRadius: 5,
                    fill: true,
                    borderWidth: 2,
                    backgroundColor: themeColor2_10
                }
            ]
        }
    });
}

function drawChart3(labels, data) {
    var chart_stat2 = document
        .getElementById("chart_stat2")
        .getContext("2d");
    if (myChart3 !== null) {
        myChart3.destroy()
    }
    myChart3 = new Chart(chart_stat2, {
        type: "LineWithShadow",
        options: {
            plugins: {
                datalabels: {
                    display: false
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [
                    {
                        gridLines: {
                            display: true,
                            lineWidth: 1,
                            color: "rgba(0,0,0,0.1)",
                            drawBorder: false
                        },
                        ticks: {
                            beginAtZero: true,
                            stepSize: parseInt(((Math.max(...data) - Math.min(...data)) / 4).toFixed(2)),
                            min: Math.min(...data),
                            max: Math.max(...data),
                            padding: 0
                        }
                    }
                ],
                xAxes: [
                    {
                        gridLines: {
                            display: false
                        }
                    }
                ]
            },
            legend: {
                display: false
            },
            tooltips: chartTooltip
        },
        data: {
            labels: labels,
            datasets: [
                {
                    label: "",
                    data: data,
                    borderColor: themeColor2,
                    pointBackgroundColor: foregroundColor,
                    pointBorderColor: themeColor2,
                    pointHoverBackgroundColor: themeColor2,
                    pointHoverBorderColor: foregroundColor,
                    pointRadius: 4,
                    pointBorderWidth: 2,
                    pointHoverRadius: 5,
                    fill: true,
                    borderWidth: 2,
                    backgroundColor: themeColor2_10
                }
            ]
        }
    });
}

function drawChart4(labels, data) {
    var chart_stat3 = document
        .getElementById("chart_stat3")
        .getContext("2d");
    if (myChart4 !== null) {
        myChart4.destroy()
    }
    myChart4 = new Chart(chart_stat3, {
        type: "LineWithShadow",
        options: {
            plugins: {
                datalabels: {
                    display: false
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [
                    {
                        gridLines: {
                            display: true,
                            lineWidth: 1,
                            color: "rgba(0,0,0,0.1)",
                            drawBorder: false
                        },
                        ticks: {
                            beginAtZero: true,
                            stepSize: parseInt(((Math.max(...data) - Math.min(...data)) / 4).toFixed(2)),
                            min: Math.min(...data),
                            max: Math.max(...data),
                            padding: 0
                        }
                    }
                ],
                xAxes: [
                    {
                        gridLines: {
                            display: false
                        }
                    }
                ]
            },
            legend: {
                display: false
            },
            tooltips: chartTooltip
        },
        data: {
            labels: labels,
            datasets: [
                {
                    label: "",
                    data: data,
                    borderColor: themeColor2,
                    pointBackgroundColor: foregroundColor,
                    pointBorderColor: themeColor2,
                    pointHoverBackgroundColor: themeColor2,
                    pointHoverBorderColor: foregroundColor,
                    pointRadius: 4,
                    pointBorderWidth: 2,
                    pointHoverRadius: 5,
                    fill: true,
                    borderWidth: 2,
                    backgroundColor: themeColor2_10
                }
            ]
        }
    });
}

function get_data(id) {
    var data = {
        csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val(),
        id: id
    }
    $.ajax({
        url: 'get_statistic',
        method: "POST",
        data: data,
        success: function (data) {
            drawChart1(data.labels1, data.data1)
            drawChart2(data.labels2, data.data2)
            drawChart3(data.labels3, data.data3)
            drawChart4(data.labels4, data.data4)
        },
        error: function (err) {
            console.log(err)
        }
    })
}



