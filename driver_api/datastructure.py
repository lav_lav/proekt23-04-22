from datetime import datetime as DT

class SensorTop():
    def  __init__(self):
        self.dataloggerUID=''
        self.UID=''
        self.deviceGroup=''
        self.deviceType=''
        self.deviceCode=''
        self.SN=''
        self.port='' #номер входа даталоггера
        
    def __str__(self):
        s='Датчик \r\n'
        s+='{u} —  УИД;\r\n'.format(u=self.UID)
        s+='{du} —  УИД даталоггера;\r\n'.format(du=self.dataloggerUID)
        s+='{g} —  группа изделия;\r\n'.format(g=self.deviceGroup)
        s+='{t} —  тип изделия; \r\n'.format(t=self.deviceType)                                                                                                   
        s+='{c} —  код изделия;\r\n'.format(c=self.deviceCode)
        s+='{sn} — серийный номер;\r\n'.format(sn=self.SN)
        s+='{ic} —  номер входа даталоггера;\r\n'.format(ic=self.port)
        return s


class DataloggerTop():
    def  __init__(self):
        self.UID=-1
        self.deviceGroup=-1
        self.deviceType=-1
        self.deviceCode=-1
        self.SN=-1
        self.IC=-1 
        self.sensorList=[]

    def __str__(self):
        s='Даталоггер \r\n'
        s+='{u} —  УИД;\r\n'.format(u=hex(self.UID))
        s+='{g} —  группа изделия;\r\n'.format(g=self.deviceGroup)
        s+='{t} —  тип изделия; \r\n'.format(t=self.deviceType)                                                                                                   
        s+='{c} —  код изделия;\r\n'.format(c=self.deviceCode)
        s+='{sn} — серийный номер;\r\n'.format(sn=self.SN)
        s+='{ic} —  код интерфейса связи (11 CAN1, 12 CAN2, 02 ZigBee);\r\n'.format(ic=self.IC)
        
        return s


class Topology():
    def __init__(self):
        self.type='TOP'
        self.version=''
        self.dt=DT.fromtimestamp(0)
        self.deviceGroup=-1
        self.deviceType=-1
        self.deviceCode=-1
        self.SN=-1
        self.UID=-1
        self.objects=-1
        self.dataloggerList=[]
    
    def __str__(self):
        s='Хост-контроллер \r\n'
        s+='TOP —  признак файла топологии;\r\n'
        s+='{v} —  номер версии документа\r\n'.format(v=self.version)
        s+='{dt} —  дата, время создания файла\r\n'.format(dt=self.dt)
        s+='{g} —  группа изделия;\r\n'.format(g=self.deviceGroup)
        s+='{t} —  тип изделия; \r\n'.format(t=self.deviceType)                                                                                                   
        s+='{c} —  код изделия;\r\n'.format(c=self.deviceCode)
        s+='{sn} — серийный номер;\r\n'.format(sn=self.SN)
        s+='{u} —  УИД;\r\n'.format(u=hex(self.UID))
        s+='{ic} —  количество даталоггеров;\r\n'.format(ic=self.objects)
        
        return s    

class SensorData():
    def __init__(self):
        self.sensor=SensorTop()
        self.timePoints=[]
        self.period=[]
        self.freg=[]
        self.temp=[]
    
    def __str__(self):
        s=self.sensor.__str__()
        for i in range(0,len(self.timePoints)):
           s+=self.timePoints[i].strftime('%d:%m:%Y %H:%M:%S')+\
               ';p={p},t={t},f={f}\r\n'.format(p=self.period[i],t=self.temp[i],f=self.freg[i])
        return s