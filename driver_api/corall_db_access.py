# -*-coding: utf-8 -*-

import os
import sqlite3
from datetime import datetime

class DbAccess():

    def __init__(self):
        self.connectionStatus = False
        self.dbFilePath = None

    def openConnection(self, dbFilePath):
        if os.path.isfile(dbFilePath) == False:
            print('Некорректный путь к файлу БД')
            return self.connectionStatus
        self.conn = sqlite3.connect(dbFilePath, check_same_thread=False)
        self.cursor = self.conn.cursor()
        if self.cursor != None:
            self.connectionStatus = True
            self.dbFilePath = dbFilePath
            self.setForegnKeysSupport()
        else:
            self.connectionStatus = False
        return self.connectionStatus

    def closeConnection(self):
        self.conn.close()
        self.connectionStatus = False
        self.dbFilePath = None

    def isConnected(self):
        return self.connectionStatus

    def dbFilePath(self):
        return self.dbFilePath

    def setForegnKeysSupport(self, flag=True):
        if flag == True:
            self.cursor.execute("PRAGMA foreign_keys = ON")
        else:
            self.cursor.execute("PRAGMA foreign_keys = OFF")
        self.conn.commit()

    # ====================================================================================================
    def get_sensor(self, sensor_UID: str):
        sInfo = dict()
        query_str = "SELECT * FROM sensor WHERE UID = :UID"
        try:
            d = {'UID': str(sensor_UID)}
            self.cursor.execute(query_str, d)
            res = self.cursor.fetchone()
            sInfo['sensor_id'] = res[0]
            sInfo['UID'] = res[1]
            sInfo['datalogger_UID'] = res[2]
            sInfo['device_group'] = res[3]
            sInfo['device_type'] = res[4]
            sInfo['SN'] = res[5]
            sInfo['datalogger_port'] = res[6]
        except Exception as ex:
            print(str(ex))
            return None
        return sInfo

    def get_all_sensors(self):
        sensors_list = list()
        for row in self.cursor.execute("SELECT * FROM sensor ORDER BY sensor_id"):
            sInfo = dict()
            sInfo['sensor_id'] = row[0]
            sInfo['UID'] = row[1]
            sInfo['datalogger_UID'] = row[2]
            sInfo['device_group'] = row[3]
            sInfo['device_type'] = row[4]
            sInfo['SN'] = row[5]
            sInfo['datalogger_port'] = row[6]
            sensors_list.append(sInfo)
        return sensors_list

    def delete_sensor(self, sensor_id: int):
        query_str = "DELETE FROM sensor WHERE sensor_id = :id"
        try:
            d = {'id': int(sensor_id)}
            self.cursor.execute(query_str, d)
            self.conn.commit()
            return True
        except Exception as ex:
            self.conn.rollback()
            print(str(ex))
            return False


    def delete_all_sensors(self):
        query_str = "DELETE FROM sensor"
        try:
            self.cursor.execute(query_str)
            self.conn.commit()
            return True
        except Exception as ex:
            self.conn.rollback()
            print(str(ex))
            return False


    def add_sensor(self, UID: str, datalogger_UID: str, device_group: int, device_type: int, SN: str,
                   datalogger_port: int):
        try:
            sql = "INSERT INTO sensor (UID, datalogger_UID, device_group, device_type, SN, datalogger_port) " \
                  "VALUES (?, ?, ?, ?, ?, ?)"
            params = (str(UID), str(datalogger_UID), int(device_group), int(device_type), str(SN), int(datalogger_port))
            self.cursor.execute(sql, params)
            self.conn.commit()
            return True
        except Exception as ex:
            self.conn.rollback()
            print(str(ex))
            return False


    def get_sensor_data(self, sensor_id: int):
        sensor_data_list = list()
        query_str = "SELECT * FROM sensor_data WHERE sensor_id = :id ORDER BY sensor_id, time_point"
        d = {'id': int(sensor_id)}
        for row in self.cursor.execute(query_str, d):
            sdInfo = dict()
            sdInfo['sensor_data_id'] = row[0]
            sdInfo['sensor_id'] = row[1]
            sdInfo['time_point'] = datetime.fromisoformat(row[2])
            sdInfo['period'] = row[3]
            sdInfo['temp'] = row[4]
            sdInfo['frequency'] = row[5]
            sensor_data_list.append(sdInfo)
        return sensor_data_list

    def is_sensor_data_exists(self, sensor_data_id: int):
        query_str = "SELECT COUNT(sensor_data_id) FROM sensor_data WHERE sensor_data_id = :id"
        for row in self.cursor.execute(query_str, {'id': int(sensor_data_id)}):
            if row[0] > 0:
                return True
            else:
                return False

    def get_all_sensors_data(self):
        sensor_data_list = list()
        for row in self.cursor.execute("SELECT * FROM sensor_data ORDER BY sensor_id, time_point"):
            sdInfo = dict()
            sdInfo['sensor_data_id'] = row[0]
            sdInfo['sensor_id'] = row[1]
            sdInfo['time_point'] = datetime.datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
            sdInfo['period'] = row[3]
            sdInfo['temp'] = row[4]
            sdInfo['frequency'] = row[5]
            sensor_data_list.append(sdInfo)
        return sensor_data_list

    def get_sensors_data_by_datetime_period(self, start: datetime, end: datetime):
        sensor_data_list = list()
        query_str = "SELECT * FROM sensor_data WHERE time_point >= :s AND time_point <= :e ORDER BY sensor_id, time_point"
        d = {'s': start, 'e': end}
        for row in self.cursor.execute(query_str, d):
            sdInfo = dict()
            sdInfo['sensor_data_id'] = row[0]
            sdInfo['sensor_id'] = row[1]
            sdInfo['time_point'] = datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
            sdInfo['period'] = row[3]
            sdInfo['temp'] = row[4]
            sdInfo['frequency'] = row[5]
            sensor_data_list.append(sdInfo)
        return sensor_data_list

    def add_sensor_data(self, sensor_id: int, time_point: datetime, period: int, temp: float, frequency: float):
        time_point_str = time_point.strftime('%Y-%m-%d %H:%M:%S')
        try:
            sql = "INSERT INTO sensor_data (sensor_id, time_point, period, temp, frequency) VALUES (?, ?, ?, ?, ?)"
            params = (int(sensor_id), str(time_point_str), int(period), float(temp), float(frequency))
            self.cursor.execute(sql, params)
            self.conn.commit()
            return True
        except Exception as ex:
            self.conn.rollback()
            print(str(ex))
            return False

    def delete_sensor_data(self, sensor_data_id: int):
        query_str = "DELETE FROM sensor_data WHERE sensor_data_id = :id"
        try:
            d = {'id': int(sensor_data_id)}
            self.cursor.execute(query_str, d)
            self.conn.commit()
            return True
        except Exception as ex:
            self.conn.rollback()
            print(str(ex))
            return False


    def delete_sensor_data_by_sensor(self, sensor_id: int):
        query_str = "DELETE FROM sensor_data WHERE sensor_id = :id"
        try:
            d = {'id': int(sensor_id)}
            self.cursor.execute(query_str, d)
            self.conn.commit()
            return True
        except Exception as ex:
            self.conn.rollback()
            print(str(ex))
            return False
#----------------------------------------------------------------------------



if __name__ == '__main__':
    db = DbAccess()
    if db.openConnection('corall_db.sqlite'):
        #db.delete_all_sensors()

        sensor = db.get_sensor(sensor_UID='11111111')
        print(sensor)
        print('')
        #db.add_sensor('11111111', '22222222', 1, 2, 'qwre5qewrd1', 5)
        for s in db.get_all_sensors():
            print(s)
        for d in db.get_sensor_data(sensor_id=1):
            print(d)
        print('')
        db.add_sensor_data(15, datetime(2019, 3, 5, 7, 23, 00), 27500, 16.97, 828.76)
        st = datetime(2019, 1, 1, 0, 0, 0)
        end = datetime(2019, 11, 1, 0, 0, 0)
        for d in db.get_sensors_data_by_datetime_period(st, end):
            print(d)
        db.add_sensor_data(12, datetime(2020, 5, 14, 3, 45, 11), 26000, 11.59, 825.93)
        #print(db.delete_sensor_data_by_sensor(12))
