# -------------Настройки FTP--------------------
FTP_HOST='127.0.0.1'
FTP_PORT=21
FTP_LOGIN='ftp_user'
FTP_PASSWORD=''

#-------------Настройки OPC-сервиса (JSON-RPC)------------
JSONRPC_SERVER_PORT=5555

#-------------Список ID наблюдаемых датчиков----------------
listSensorUID=['260AD56F01000083',\
                '2606DC6F0100000D',\
                '26E2D05F01000091',\
                '26B3FB6F01000048',\
                '266AD36F010000DC',\
                '26CECE6F010000CC',\
                '26D5DA5F01000044',\
                '26B0C25F0100002E']

START_MONITORING_DATETIME = '2017-01-01 00:00:00'
INTERVAL = 30