from jsonrpcclient import request, parse, Ok
import logging
import requests

from datetime import datetime as DT

class OpcServiceClient:
    def __init__(self, ip_addr=str(), port=int()):
        self.ip = ip_addr
        self.port = port

    def call_api_method(self, method_name: str, params: list = []):
        addr = 'http://{}:{}/'.format(self.ip, self.port)
        try:
            if len(params) == 0:
                response = requests.post(addr, json=request(method_name))
            else:
                params = tuple(params)
                response = requests.post(addr, json=request(method_name, params=params))
            parsed = parse(response.json())
            if isinstance(parsed, Ok):
                return parsed.result
            else:
                logging.error(parsed.message)
                return parsed.message
        except Exception as ex:
            logging.error(ex)
            return ex


    def test_connection(self):
        """
        Метод проверки соединения с JsonRpc-сервисом
        :return: bool
        """
        return bool(self.call_api_method('ping'))


    def get_api_methods(self):
        """
        Метод возвращает список доступных методов API
        :return: list[str]
        """
        return self.call_api_method('get_api_methods')


    def test_method_with_params(self, params_list):
        return self.call_api_method('test_params_method', params_list)

    def update_data(self, date_time: str):
        #dt = DT(2020, 1, 1, 2, 24, 0).strftime()
        return self.call_api_method('update_data', [date_time])


c = OpcServiceClient('127.0.0.1', 5555)
print(c.test_connection())
print(c.test_method_with_params( [1000, [1,2,3,4,5,6,7]] ))
print('Методы API:')
for m in c.get_api_methods():
    print(m)
print('Тест прямого получения данных через ftp-драйвер')
ddd = DT(2020, 1, 1, 2, 24, 0).isoformat()
print(ddd)
#d = c.update_data(ddd)
#print(d)
c.call_api_method('start_opc_server')

import time
time.sleep(10)

print(c.call_api_method('stop_opc_server'))