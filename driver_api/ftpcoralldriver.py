# -*-coding:utf-8-*-

import driver_api.coralllibrary as CL

class FtpCorallDriver:
    _connected = False
    _ftp = None

    def ftp_connect(self, host='', port='', login='', password=''):
        ftp = CL.openFTP(host, port, login, password)
        if ftp:
            self._connected = True
            self._ftp = ftp
            return ftp
        else:
            self._connected = False
            return False

    def connected(self):
        return self._connected

    async def get_sensor_data(self, start_date=None, sensor_UID_list=[]):
        if self.connected():
            return CL.getSensorData(self._ftp, start_date, sensor_UID_list)
        else:
            return None
