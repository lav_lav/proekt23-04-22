import settings as S

from datetime import datetime as DT
import coralllibrary as CL


listSensorUID=['260AD56F01000083',\
                '2606DC6F0100000D',\
                '26E2D05F01000091',\
                '26B3FB6F01000048',\
                '266AD36F010000DC',\
                '26CECE6F010000CC',\
                '26D5DA5F01000044',\
                '26B0C25F0100002E']

class FtpCorallDriver:
    _connected = False
    _ftp = None

    def ftp_connect(self, host='', port='', login='', password=''):
        ftp = CL.openFTP(host, port, login, password)
        if ftp:
            self._connected = True
            self._ftp = ftp
            return ftp
        else:
            self._connected = False
            return False

    def connected(self):
        return self._connected

    def get_sensor_data(self, start_date=None, sensor_UID_list=[]):
        if self.connected():
            return CL.getSensorData(self._ftp, start_date, sensor_UID_list)
        else:
            return None


if __name__ == '__main__':

    ftpDrv = FtpCorallDriver()
    f = ftpDrv.ftp_connect(S.FTP_HOST, S.FTP_PORT, S.FTP_LOGIN, S.FTP_PASSWORD)
    if f is not False:
        data = ftpDrv.get_sensor_data(DT(2020, 1, 1, 2, 24, 0), listSensorUID)

    '''
    s = shelve.open('data.db')
    s['data'] = data
    s.close()
    s = shelve.open('data.db')
    data1 = s['data']
    s.close()
    '''
    
    for d in data:
        print(d.__str__())