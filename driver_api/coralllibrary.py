from datetime import datetime as DT
from ftplib import FTP
import io
import gzip
import driver_api.datastructure as D


# ------------------------------------------------------------------------------
def openFTP(host='127.0.0.1', port=21, login='login', password='password'):
    ftp = FTP()
    ftp.connect(host, port)
    try:
        ftp.login(login, password)
    except Exception as ex:
        print(ex)
        return False
    return ftp


# ------------------------------------------------------------------------------
def getFileNames(ftp):
    return ftp.nlst()


# -------------------------------------------------------------------------------
def getStringsFromText(text=str()):
    """Возвращает двумерный массив из блока текста text
        первый индекс - номер строки, второй - номер тега в строке"""
    strings = []
    string = ''
    cursor = 1
    for index in range(4, len(text)):
        if text[index - 2:index] == '\r\n':

            if text[index - 3] == '>' or text[index - 3] == '#':
                strings.append(text[cursor:index - 3])
            else:
                strings.append(text[cursor:index - 2])
            cursor = index
        if index == len(text) - 1:
            strings.append(text[cursor + 1:index - 2])
    tags = []
    for string in strings:
        stringTags = []
        cursor = -1
        for index in range(0, len(string)):
            if string[index] == ';':
                stringTags.append(string[cursor + 1:index])
                cursor = index
            if index == len(string) - 1:
                stringTags.append(string[cursor + 1:])
        tags.append(stringTags)
    return tags


# ----------------------------------------------------------------------------
def getDTFromFileName(name=[]):
    """Возвращает дату из имени файла"""
    if name == '':
        dt = DT.today()
    else:
        dt = DT.strptime(name[:17], '%d%m%Y_%H_%M_%S')
    return dt


# -----------------------------------------------------------------------------
def getLastSPR(ftp, startDT=DT.now()):
    """Возвращает массив имен файлов .spr, содержащих данные после startDT
        Если startDT не установлен, то для текущего времени"""

    lastFileList = []
    fileList = ftp.nlst()
    for name in fileList:
        if name[-6:-3] == 'spr':
            if getDTFromFileName(name) <= startDT:
                lastFileName = name
                break

    for name in fileList:
        if name[-6:-3] == 'spr':
            if getDTFromFileName(name) <= startDT and getDTFromFileName(name) > getDTFromFileName(lastFileName):
                lastFileName = name
            if getDTFromFileName(name) > startDT:
                lastFileList.append(name)
    lastFileList.append(lastFileName)
    #print(lastFileList)
    return lastFileList


# -----------------------------------------------------------------------------

def getLastTop(ftp, lastDT=DT.today()):
    """Возвращает имя последнего файла топологии
        Если lastDT установлен, то последнего файла до верхней границы lastDT"""
    lastFileName = ''
    fileList = ftp.nlst()
    for name in fileList:
        if name[-6:-3] == 'top':
            if getDTFromFileName(name) < lastDT:
                lastFileName = name
                break

    for name in fileList:
        if name[-6:-3] == 'top':
            if getDTFromFileName(name) < lastDT and getDTFromFileName(name) > getDTFromFileName(lastFileName):
                lastFileName = name
    #print(lastFileName)
    return lastFileName


# -----------------------------------------------------------------------------

def getTextFromFile(ftp, fileName):
    """Возвращает блок текста из файла"""
    binary = io.BytesIO()
    ftp.retrbinary("RETR " + fileName, callback=binary.write)
    binary.seek(0)  # Go back to the start
    arh = gzip.GzipFile(fileobj=binary)
    textFile = arh.read().decode('utf-8')
    return textFile


# -----------------------------------------------------------------------------

def getSensorData(ftp, timePoint=DT.now(), listSensorUID=[]):
    listSensorData = []
    fileTop = getLastTop(ftp, timePoint)
    textTop = getTextFromFile(ftp, fileTop)
    strTop = getStringsFromText(textTop)
    # ----- Инициализация-----
    for uid in listSensorUID:
        for string in strTop:
            if len(string) > 2:  # не смотреть на строку datalogger
                if uid == string[1]:
                    sensorData = D.SensorData()
                    sensorData.sensor.UID = uid
                    sensorData.sensor.dataloggerUID = string[0]
                    sensorData.sensor.deviceGroup = string[2][:2]
                    sensorData.sensor.deviceType = string[2][3:5]
                    sensorData.sensor.deviceCode = string[2][6:]
                    sensorData.sensor.SN = string[3]
                    sensorData.sensor.port = string[4]
                    listSensorData.append(sensorData)

    listSPR = getLastSPR(ftp, timePoint)
    for spr in listSPR:
        strSPR = getStringsFromText(getTextFromFile(ftp, spr))
        for string in strSPR:
            for sensorData in listSensorData:
                if sensorData.sensor.UID == string[2]:
                    stringDT = DT.strptime(string[0] + ';' + string[1], '%d.%m.%Y;%H:%M:%S')  # ????
                    if stringDT > timePoint:
                        sensorData.timePoints.append(stringDT)
                        sensorData.period.append(string[5])
                        sensorData.freg.append(string[8])
                        sensorData.temp.append(string[9])

    return listSensorData
