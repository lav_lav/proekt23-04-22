#-*-coding:utf-8-*-

from jsonrpcserver import method, Result, Success, Error, serve
import driver_api.settings as sett

from datetime import datetime as DT

from driver_api.ftpcoralldriver import *

import asyncio
import driver_api.opcserver as o

api_methods = ["ping() -> Result",
                "get_api_methods() -> Result",
                "test_params_method(a: int, b: list) -> Result"]


class OpcService:
    def start(self, opc_service_port=5555):
        serve(port=port)

@method
def ping() -> Result:
    return Success(True)

@method
def get_api_methods() -> Result:
    return Success(api_methods)

@method
def test_params_method(a: int, b: list) -> Result:
    d = {'a': a, 'b': b}
    return Success(d)

@method
def update_data(last_datetime: str) -> Result:
    dt = DT.fromisoformat(last_datetime)
    if ftpDrv.connected():
        data = ftpDrv.get_sensor_data(dt, sett.listSensorUID)
        return Success(data)
    else:
        return Error('Ошибка ftp-соединения с контроллером')

@method
def start_opc_server() -> Result:
    asyncio.run(o.main())
    return True

@method
def stop_opc_server() -> Result:
    return asyncio.run(o.stop_server())



if __name__ == "__main__":
    ftpDrv = FtpCorallDriver()
    ftpDrv.ftp_connect(S.FTP_HOST, S.FTP_PORT, S.FTP_LOGIN, S.FTP_PASSWORD)


    port = S.JSONRPC_SERVER_PORT
    service = OpcService()

    import socket

    print(socket.gethostbyname(socket.gethostname()))

    print('OPC-сервис запущен на порту: {}'.format(port))
    service.start(opc_service_port=port)


    '''
    if f is not False:
        data = ftpDrv.get_sensor_data(DT(2020, 1, 1, 2, 24, 0), listSensorUID)
    '''
    #serve(port=5555)